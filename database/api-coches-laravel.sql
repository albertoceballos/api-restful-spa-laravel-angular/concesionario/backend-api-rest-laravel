-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2019 a las 10:30:40
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `api-coches-laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `description` text COLLATE utf8_spanish_ci,
  `price` int(30) DEFAULT NULL,
  `status` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cars`
--

INSERT INTO `cars` (`id`, `user_id`, `title`, `description`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dacia Logan', 'Coche gama baja', 6000, NULL, '2019-05-14 08:17:08', '2019-05-14 08:17:08'),
(2, 1, 'Mercedes Clase C', 'Coche gama alta', 33000, NULL, '2019-05-14 08:18:50', '2019-05-14 09:29:37'),
(3, 1, 'Mercedes Clase A', 'Coche gama media', 15000, NULL, '2019-05-14 09:57:39', '2019-05-14 09:57:40'),
(4, 1, 'Ford Kuga 1.2', 'Utilitario de la marca norteamericana de buena calidad/precio', 7150, 'true', '2019-05-16 11:49:45', '2019-05-17 09:41:51'),
(5, 1, 'Renault Clio', 'Utlitario perfecto para la ciudad', 7300, 'true', '2019-05-16 11:58:26', '2019-05-16 11:58:26'),
(8, 1, 'Porsche Carrera', 'deportivo, 3 puertas, negro', 58000, 'false', '2019-05-16 19:37:32', '2019-05-16 19:37:32'),
(9, 1, 'VolskWagen Sirocco', 'Deportivo, 3 puertas, blanco', 14500, 'false', '2019-05-16 19:41:12', '2019-05-16 19:41:12'),
(10, 3, 'Mitsubishi Lancer', '4 puertas, rojo, alerón', 28000, 'true', '2019-05-17 10:17:12', '2019-05-17 10:19:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `surname`, `email`, `password`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'ROLE_USER', 'Alberto', 'Ceballos', 'berto@berto.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '2019-05-13 11:24:57', '2019-05-20 08:48:44', NULL),
(2, 'ROLE_USER', 'Pedro', 'Martínez', 'pedro@pedro.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '2019-05-15 09:12:10', '2019-05-15 09:12:10', NULL),
(3, 'ROLE_USER', 'Juan', 'Rodrguez', 'juan@juan.com', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '2019-05-15 09:14:25', '2019-05-15 09:14:25', NULL),
(4, 'ROLE_USER', 'María', 'Gómez', 'maria@maria.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2019-05-15 09:26:19', '2019-05-15 09:26:19', NULL),
(6, 'ROLE_USER', 'Carlos', 'Gómez', 'carlos@carlos.es', '7b85175b455060e3237e925f023053ca9515e8682a83c8b09911c724a1f8b75f', '2019-05-18 09:44:29', '2019-05-20 08:54:17', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cars`
--
ALTER TABLE `cars`
  ADD CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
