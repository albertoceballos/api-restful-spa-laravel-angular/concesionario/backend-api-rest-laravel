<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});



Route::prefix('api')->group(function() {

    //UserController
    Route::post('/register', [
        'as' => 'register',
        'uses' => 'UserController@register',
    ]);

    Route::post('/login', [
        'as' => 'login',
        'uses' => 'UserController@login',
    ]);
    
    Route::post('/update','UserController@update');
    Route::get('/user','UserController@getLogguedUser');
    
    //CarController
    Route::get('/cars','CarController@index');
    
    Route::post('/car/new','CarController@storeCar');
    
    Route::get('/car/show/{carId}','CarController@showCar');
    
    Route::post('/car/update/{carId}','CarController@updateCar');
    
    Route::get('car/delete/{carId}','CarController@deleteCar');
    
    Route::get('search/{searchString}/{order?}','CarController@search');
    
    Route::get('order/{order}','CarController@orderAll');
});


