<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//servicio
use App\Helpers\JwtAuth;
use App\User;

class UserController extends Controller {

    public function register(Request $request) {
        $json = $request->input('json', null);
        $params = json_decode($json);

        if (!empty($json)) {
            $name = !empty($params->name) ? $params->name : null;
            $surname = !empty($params->surname) ? $params->surname : null;
            $email = !empty($params->email) ? $params->email : null;
            $password = !empty($params->password) ? $params->password : null;

            if (!empty($name) && !empty($email) && !empty($password)) {
                $user = new User();
                $user->name = $name;
                $user->surname = $surname;
                $user->role = "ROLE_USER";
                $user->email = $email;
                $encrypted_password = hash('sha256', $password);
                $user->password = $encrypted_password;
                $user->created_at = new \DateTime('now');

                //comprobar duplicados
                $isset_user = User::where('email', $email)->get();
                //si no encuentra, devuelvo el error sino encuentra (es igual a 0)
                if (count($isset_user) != 0) {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Ya existe un usuario con ese e-mail',
                    ];
                } else {
                    $user->save();
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Usuario guardado con éxito',
                    ];
                }
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Faltan datos',
                ];
            }
        }
        return response()->json($data);
    }

    public function login(Request $request, JwtAuth $jwtAuth) {

        $json = $request->input('json', null);
        $params = json_decode($json);
        if (!empty($json)) {
            $email = !empty($params->email) ? $params->email : null;
            $password = !empty($params->password) ? $params->password : null;
            $getToken = !empty($params->getToken) ? $params->getToken : null;

            $encoded_password = hash('sha256', $password);
            if (!empty($email) && !empty($encoded_password)) {

                if (empty($getToken)) {
                    $signup = $jwtAuth->signup($email, $encoded_password);
                } else {
                    $signup = $jwtAuth->signup($email, $encoded_password, $getToken);
                }
            } else {
                $signup = [
                    'status' => 'error',
                    'message' => 'Faltan datos',
                ];
            }
        }

        return response()->json($signup, 200);
    }

    public function update(Request $request, JwtAuth $jwtAuth) {
        //consigo token
        $token = $request->header('Authorization');
        //compruebo token
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            //si es correcto el token, consigo la identidad del usuario
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;
            //objeto de usuario a actualizar
            $user = User::find($user_id);
            if (is_object($user)) {
                //conseguir datos a actualizar
                $json = $request->input('json', null);
                $params = json_decode($json);
                $params_array = json_decode($json, true);
                if (!empty($json)) {
                    $validator = \Validator::make($params_array, [
                                'name' => 'required',
                                'email' => 'required',
                                'password' => 'required|min:5',
                    ]);
                    if ($validator->fails()) {
                        $data = [
                            'code' => 400,
                            'status' => 'error',
                            'error_messages' => $validator->errors(),
                        ];
                    } else {
                        $user->name = $params->name;
                        $user->surname = !empty($params->surname) ? $params->surname : null;
                        $user->updated_at=new \DateTime('now');
                        $pwd = hash('sha256', $params->password);
                        $user->password = $pwd;
                        //comprobar email si está duplicado
                        $isset_user = User::where('email', $params->email)->first();
                        //si no existe el email o es el mismo que ya tiene el usuario
                        if (empty($isset_user) || $isset_user->email == $user->email) {
                            $user->email = $params->email;
                            $user->save();
                            $data = [
                                'code' => 200,
                                'status' => 'success',
                                'message' => 'Usuario actualizado con éxito',
                                'user' => $user,
                            ];
                        } else {
                            $data = [
                                'code' => 400,
                                'status' => 'error',
                                'message' => 'El usuario ya existe',
                            ];
                        }
                    }
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Faltan datos',
                    ];
                }
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'No existe el usuario',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
        }
        return response()->json($data);
    }

}
