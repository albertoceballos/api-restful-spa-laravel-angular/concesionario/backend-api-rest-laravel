<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//servicio JwtAuth
use App\Helpers\JwtAuth;
//modelos
use App\Car;

class CarController extends Controller {

    //listado de todos los coches
    public function index() {
        $cars = Car::all()->load('user');
        $data = [
            'code' => 200,
            'status' => 'success',
            'cars' => $cars,
        ];
        return response()->json($data);
    }

    //coche por su Id
    public function showCar($carId) {
        $car = Car::find($carId);

        if (is_object($car)) {
            $car = Car::find($carId)->load('user');
            $data = [
                'code' => 200,
                'status' => 'success',
                'car' => $car,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No existe el coche',
            ];
        }
        return response()->json($data);
    }

    //guardar nuevo coche
    public function storeCar(Request $request, JwtAuth $jwtAuth) {
        $token = $request->header('Authorization', null);
        $auth = $jwtAuth->checkToken($token);


        if ($auth) {
            //consigo la identidad del usuario, del token descodificado
            $user = $jwtAuth->checkToken($token, true);
            //consigo los datos del coche a guardar
            $json = $request->input('json', null);
            $params = json_decode($json);
            //array para la validación
            $params_array = json_decode($json, true);
            //valido los datos que me llegan 
            //mensajes de validación:
            $messages = [
                'title.required' => 'El título es necesario',
                'price.required' => 'El precio es necesario',
                'title.min' => 'El título debe tener al menos 5 caracteres'
            ];

            //creo el validator con las reglas y le paso los mensajes;
            $validator = \Validator::make($params_array, [
                        'title' => 'required|min:5',
                        'price' => 'required|numeric',
                            ], $messages);

            //si hay errores devuelve los errores;
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            //si la validación es correcta, crear nuevo objeto coche y le seteo los nuevos valores
            $car = new Car();
            $car->title = $params->title;
            $car->user_id = $user->sub;
            $car->description = !empty($params->description) ? $params->description : null;
            $car->price = $params->price;
            $car->status = !empty($params->status) ? $params->status : null;
            $car->created_at = new \DateTime('now');
            //guardo el objeto
            $car->save();

            $data = [
                'code' => 200,
                'status' => 'success',
                'message' => 'Nuevo coche guardado con éxito',
                'car' => $car,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación inválida',
            ];
        }
        return response()->json($data);
    }

    //actualizar coche
    public function updateCar($carId, Request $request, JwtAuth $jwtAuth) {
        $token = $request->header('Authorization', null);
        $auth = $jwtAuth->checkToken($token);

        if ($auth) {

            $json = $request->input('json');
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            //valido los datos que me llegan 
            //mensajes de validación:
            $messages = [
                'title.required' => 'El título es necesario',
                'price.required' => 'El precio es necesario',
                'title.min' => 'El título debe tener al menos 5 caracteres'
            ];

            //creo el validator con las reglas y le paso los mensajes;
            $validator = \Validator::make($params_array, [
                        'title' => 'required|min:5',
                        'price' => 'required|numeric',
                            ], $messages);

            //si hay errores devuelve los errores;
            if ($validator->fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'errors' => $validator->errors(),
                ];
            } else {
                //sacar objeto del coche a actualizar
                $car = Car::find($carId);
                //setear nuevos parámetros;
                $car->title = $params->title;
                $car->description = !empty($params->description) ? $params->description : null;
                $car->price = $params->price;
                $car->status = !empty($params->status) ? $params->status : null;
                $car->updated_at = new \DateTime('now');
                //guardar
                $car->save();
                //devolver datos
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Coche actualizado con éxito',
                    'car' => $car
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación inválida',
            ];
        }
        return response()->json($data);
    }

    //eliminar coche
    public function deleteCar($carId, Request $request, JwtAuth $jwtAuth) {
        $token = $request->header('Authorization', null);
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            $car = Car::find($carId);
            $car->delete();
            $data = [
                'code' => 200,
                'status' => 'success',
                'message' => 'Coche borrado correctamente',
                'car' => $car,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación inválida'
            ];
        }
        return response()->json($data);
    }

    public function search($searchString = null, $order = 'new') {
        if (!empty($searchString)) {
            switch ($order) {
                case 'new':
                    $column = 'id';
                    $filter = 'DESC';
                    break;
                case 'old':
                    $column = 'id';
                    $filter = 'ASC';
                    break;
                case 'alfa-desc':
                    $column = 'title';
                    $filter = 'DESC';
                    break;
                case 'alfa-asc':
                    $column = 'title';
                    $filter = 'ASC';
                    break;
                case 'price-asc':
                    $column = 'price';
                    $filter = 'ASC';
                    break;
                case 'price-desc':
                    $column = 'price';
                    $filter = 'DESC';
                    break;
                default :
                    $column = 'id';
                    $filter = 'ASC';
                    break;
            }

            $cars = Car::where('title', 'LIKE', '%' . $searchString . '%')->orWhere('description', 'LIKE', '%' . $searchString . '%')
                    ->orderBy($column, $filter)
                    ->get();

            if (count($cars) > 0) {
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'cars' => $cars,
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Sin resultados en la búsqueda',
                ];
            }
        }

        return response()->json($data);
    }

    public function orderAll($order = 'new') {
            switch ($order) {
                case 'new':
                    $column = 'id';
                    $filter = 'DESC';
                    break;
                case 'old':
                    $column = 'id';
                    $filter = 'ASC';
                    break;
                case 'alfa-desc':
                    $column = 'title';
                    $filter = 'DESC';
                    break;
                case 'alfa-asc':
                    $column = 'title';
                    $filter = 'ASC';
                    break;
                case 'price-asc':
                    $column = 'price';
                    $filter = 'ASC';
                    break;
                case 'price-desc':
                    $column = 'price';
                    $filter = 'DESC';
                    break;
                default :
                    $column = 'id';
                    $filter = 'ASC';
                    break;
            }

            $cars = Car::orderBy($column, $filter)->get();

            if (count($cars) > 0) {
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'cars' => $cars,
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Sin resultados en la búsqueda',
                ];
            }

        return response()->json($data);
    }

}
