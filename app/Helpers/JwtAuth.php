<?php

namespace App\Helpers;

use Firebase\JWT\JWT;

use App\User;


class JwtAuth{
    public $secret_key;
    
    public function __construct() {
        $this->secret_key="Una_clave_secreta9999$";
    }
    
    public function signup($email,$password,$getToken=null){
        //extarer objeto de usuario
        $user= User::where([
            'email'=>$email,
            'password'=>$password
        ])->first();
        
        $signup=false;
        if(!empty($user)){
            $signup=true;
        }
        if($signup){
            //Generar el token y devolver
            $token=[
              'sub'=>$user->id,
              'email'=>$user->email,
              'name'=>$user->name,
              'surname'=>$user->surname,
              'iat'=>time(),
              'exp'=>time()+(60*60*24*7),
            ];
            
            //token codificado
            $jwt= JWT::encode($token, $this->secret_key, 'HS256');
            //token decodificado
            $decoded= JWT::decode($jwt, $this->secret_key, ['HS256']);
            
            //Si mandamos getToken,devuelven el token codificado, sino devuelve los datos codificados
            if(!empty($getToken)){
                return $jwt;
            }else{
                return $decoded;
            }
            
        }else{
            return ['status'=>'error','message'=>'El login ha fallado'];
        }
        
    }
    
    public function checkToken($jwt,$getIdentity=null){
        $auth=false;
        
        try{
            $decoded= JWT::decode($jwt, $this->secret_key,['HS256']);
        } catch (\UnexpectedValueException $ex) {
            $auth=false;
        } catch (\DomainException $ex){
            $auth=false;
        }
        
        if(!empty($decoded) && isset($decoded->sub)){
            $auth=true;
        }else{
            $auth=false;
        }
        
        if($getIdentity){
            return $decoded;
        }
        
        return $auth;
    }
    
}
