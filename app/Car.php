<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table='cars';
    
    //relación Many to One
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
